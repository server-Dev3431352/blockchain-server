# First commit

## Spring Security Refresh Token with JWT in Spring Boot example

Build JWT Refresh Token with Spring Security in the Spring Boot Application. You can know how to expire the JWT Token, then renew the Access Token with Refresh Token in HttpOnly Cookie.

## User Registration, User Login and Authorization process

Authentication JWT (Login, Sign Out, Registration, Refresh Token)

## Run following SQL insert statements after run project once

```
INSERT INTO roles(name) VALUES('ROLE_CUSTOMER');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');
INSERT INTO roles(name) VALUES('ROLE_STAFF');
INSERT INTO roles(name) VALUES('CONTROLLER');
```

## Run Spring Boot application

```cli
mvn spring-boot:run

```

# Version

````text
0.0.1
````
