package com.server.blockchainserver.platform.entity.enums;

public enum TransactionStatus {
    PENDING,
    CONFIRMED,
    REJECTED,
    COMPLETED
}
