package com.server.blockchainserver.platform.entity.enums;

public enum TransactionType {
    BUY,
    SELL;
}
