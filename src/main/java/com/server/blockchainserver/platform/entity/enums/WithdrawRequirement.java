package com.server.blockchainserver.platform.entity.enums;

public enum WithdrawRequirement {
    AVAILABLE,
    CRAFT,
}
