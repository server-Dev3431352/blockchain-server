package com.server.blockchainserver.platform.entity.enums;

public enum ContractStatus {
    DIGITAL_SIGNED,
    DIGITAL_UNSIGNED
}
