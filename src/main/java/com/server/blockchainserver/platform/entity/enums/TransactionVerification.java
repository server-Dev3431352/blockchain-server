package com.server.blockchainserver.platform.entity.enums;

public enum TransactionVerification {
    UNVERIFIED,
    VERIFIED
}
