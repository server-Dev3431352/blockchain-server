package com.server.blockchainserver.platform.entity.enums;

public enum EUserConfirmWithdraw {
    NOT_RECEIVED,
    RECEIVED
}
