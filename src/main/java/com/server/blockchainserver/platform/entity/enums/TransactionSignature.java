package com.server.blockchainserver.platform.entity.enums;

public enum TransactionSignature {
    //Signature Status
    UNSIGNED,
    SIGNED
}
