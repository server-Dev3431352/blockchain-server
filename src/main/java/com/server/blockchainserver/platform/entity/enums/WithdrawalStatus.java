package com.server.blockchainserver.platform.entity.enums;

public enum WithdrawalStatus {
    UNVERIFIED,
    PENDING,
    CONFIRMED,
    COMPLETED,
    CANCELLED,
    REJECTED,
    APPROVED
}
