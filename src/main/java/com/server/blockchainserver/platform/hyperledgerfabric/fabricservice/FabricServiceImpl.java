package com.server.blockchainserver.platform.hyperledgerfabric.fabricservice;

import com.server.blockchainserver.platform.hyperledgerfabric.fabricservice.FabricServices;
import com.server.blockchainserver.platform.hyperledgerfabric.network.FabricGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FabricServiceImpl implements FabricServices {

    @Autowired
    FabricGateway fabricGateway;

    @Override
    public String getAllAsset() throws Exception{
        var result = fabricGateway.getAllAssets();
        return result;
    }
}
