
package com.server.blockchainserver.controllers.fabric_controller;

import com.server.blockchainserver.platform.hyperledgerfabric.fabricservice.FabricServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class FabricController {

    @Autowired
    FabricServices services;


    @GetMapping("/get-all-assets")
    public String getAllAssets() throws Exception {
        return services.getAllAsset();
    }

}


