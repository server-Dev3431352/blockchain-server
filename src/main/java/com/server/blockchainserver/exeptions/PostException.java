package com.server.blockchainserver.exeptions;

public class PostException extends RuntimeException {
    public PostException(String message) {
        super(message);
    }
}
