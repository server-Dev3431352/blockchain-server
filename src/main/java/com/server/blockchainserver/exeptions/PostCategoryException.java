package com.server.blockchainserver.exeptions;

public class PostCategoryException extends RuntimeException {
    public PostCategoryException(String message) {
        super(message);
    }
}
