package com.server.blockchainserver.exeptions;

public class UserInfoException extends RuntimeException {
    public UserInfoException(String message) {
        super(message);
    }
}
