package com.server.blockchainserver.exeptions;

public class ProductException extends RuntimeException {
    public ProductException(String message) {
        super(message);
    }
}
