package com.server.blockchainserver.exeptions;

public class ReviewProductException extends RuntimeException {
    public ReviewProductException(String message) {
        super(message);
    }
}
