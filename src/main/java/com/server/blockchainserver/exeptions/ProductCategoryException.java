package com.server.blockchainserver.exeptions;

public class ProductCategoryException extends RuntimeException {

    public ProductCategoryException(String message) {
        super(message);
    }
}
