package com.server.blockchainserver.exeptions;

public class TypeGoldException extends RuntimeException{

    public TypeGoldException(String message){
        super(message);
    }
}
