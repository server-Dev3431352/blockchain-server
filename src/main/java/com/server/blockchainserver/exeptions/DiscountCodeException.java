package com.server.blockchainserver.exeptions;

public class DiscountCodeException extends RuntimeException {
    public DiscountCodeException(String message) {
        super(message);
    }
}
