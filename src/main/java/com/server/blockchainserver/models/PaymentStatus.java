package com.server.blockchainserver.models;


public enum PaymentStatus {

    PENDING,
    PAID,
    NOT_PAID,
    CANCELLED,
}
