package com.server.blockchainserver.models.enums;

public enum PaymentStatus {
    SUCCESS,
    FAILED,
    CANCEL
}
