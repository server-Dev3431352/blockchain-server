package com.server.blockchainserver.models.enums;

public enum EReceivedStatus {
    UNVERIFIED,
    RECEIVED, // đã nhận
    NOT_RECEIVED // chưa nhận
}
