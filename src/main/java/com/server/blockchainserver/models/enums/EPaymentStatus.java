package com.server.blockchainserver.models.enums;

public enum EPaymentStatus {
    PAID,
    NOT_PAID
}
