package com.server.blockchainserver.models.enums;

public enum EUserConfirm {
    NOT_RECEIVED,
    RECEIVED
}
